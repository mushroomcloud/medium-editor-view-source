(function (root, factory) {
    'use strict';
    if (typeof module === 'object') {
        module.exports = factory;
    } else if (typeof define === 'function' && define.amd) {
        define(factory);
    } else {
        root.ViewSourceButton = factory;
    }
}(this, function (MediumEditor) {
  var ViewSourceButton = MediumEditor.extensions.button.extend({
    name: 'view-source',
    aria: "View Source",
    contentFA: '<i class="fa fa-code"></i>',
    contentDefault: '<b>Source</b>',
    hideCode: function(event){
      this.setInactive();
      if (event && event.relatedTarget == this.button){
        return;
      }
      this.viewSource = false;
      var self = this;
      toolbar = this.base.getExtensionByName("toolbar");
      toolbar.sticky = this.base.options.toolbar.sticky;
      toolbar.static = this.base.options.toolbar.static;
      toolbar.align = this.base.options.toolbar.align;
      toolbar.destroy();
      toolbar.init();
      this.base.off(this.base.getFocusedElement().children[0], "blur", this.hideCode);
      this.base.extensions.forEach(function(ext){
        if(ext != self && ext.button){
          ext.button.removeAttribute("disabled");
          ext.button.style.color = "";
        }
      });
      this.base.getFocusedElement().innerHTML = this.base.getFocusedElement().children[0].value;
    },
    handleClick: function(event) {
      var self = this;
      this.viewSource = !this.viewSource;
      if(this.viewSource){
        this.setActive();
        var toolbar = this.base.getExtensionByName("toolbar");
        toolbar.sticky = true;
        toolbar.static = true;
        toolbar.align = "left";
        toolbar.destroy();
        toolbar.init();
        /*this.originalButtons = this.getEditorOption("toolbar").buttons;
        this.base.options.toolbar.buttons = ["view-source"];*/
        this.base.extensions.forEach(function(ext){
          if(ext != self && ext.button){
            ext.button.setAttribute("disabled","disabled");
            ext.button.style.color = "grey";
          }
        });
        this.base.getFocusedElement().innerHTML = "<textarea>" + this.base.getFocusedElement().innerHTML.trim() + "</textarea>";
        this.base.getFocusedElement().children[0].focus();
        this.base.on(this.base.getFocusedElement().children[0], "blur", this.hideCode.bind(this));
      }
      else{
        this.hideCode();
      }
      this.base.getExtensionByName("toolbar").setToolbarPosition();
      this.base.getFocusedElement().setAttribute("data-view-source", this.viewSource ? "true" : "false");
    },
    isActive: function(){
      return this.viewSource;
    },
    isAlreadyApplied: function(node){
        return this.viewSource;
    }
  });
  return ViewSourceButton;
}(typeof require === 'function' ? require('medium-editor') : MediumEditor)));
